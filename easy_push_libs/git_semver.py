"""
Copyright (c) 2017 Fernando Collova

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""

from easy_push_libs.libs import handle_user_input
from git import Repo, TagReference
from git.refs.head import HEAD
import re


class SemverHead(HEAD):

    @property
    def is_master(self):
        return str(self.ref) == "master"

    def is_minor_version_of(self, tag):
        return str(self.ref) == tag.minor_version


class SemverTag():

    def __init__(self, tag):

        valid_tag_regex = re.compile(r'\d+.\d+.\d+')
        match_result = valid_tag_regex.match(tag)
        if not match_result:
            raise ValueError('{} is not  a valid tag'.format(tag))

        tag_parts = tag.split(".")

        self.major_number = int(tag_parts[0])
        self.minor_number = int(tag_parts[1])
        self.bugfix_number = int(tag_parts[2])

    @property
    def is_bugfix(self):
        return self.bugfix_number != 0

    @property
    def is_minor(self):
        return self.bugfix_number == 0

    @property
    def is_major(self):
        return self.minor_number == 0

    @property
    def minor_version(self):
        return ".".join([str(self.major_number), str(self.minor_number)])

    @property
    def major_version(self):
        return str(self.major_number)

    @property
    def bugfix_version(self):
        return str(self)

    def __str__(self):

        return ".".join([
            str(self.major_number),
            str(self.minor_number),
            str(self.bugfix_number),
        ])

    def interactive_bump(self):
        """Return a new tag object with a release number increased."""

        next_bugfix_version = ".".join([
            str(self.major_number),
            str(self.minor_number),
            str(self.bugfix_number + 1),
        ])

        next_minor_version = ".".join([
            str(self.major_number),
            str(self.minor_number + 1),
            "0",
        ])

        next_major_version = ".".join([
            str(self.major_number + 1),
            "0",
            "0",
        ])

        releases_choices = {
            "b": next_bugfix_version,
            "m": next_minor_version,
            "M": next_major_version
        }

        user_choice = handle_user_input(
            "Last tag is {}, and this are the releases you can choose from:\n"
            "Choose the letter that fits the release type\n\n"
            "{} (b)ugfix release\n"
            "{} (m)inor release\n"
            "{} (M)ajor release\n\n"
            "Or (a) to abort\n\n"
            "Your choice?: ".format(
                str(self),
                next_bugfix_version,
                next_minor_version,
                next_major_version,
            ),
            ["a", "b", "m", "M"]
        )

        return self.__class__(releases_choices[user_choice])


class SemverRepo(Repo):

    @property
    def last_tag(self):
        return sorted(
            self.tags,
            key=lambda t: t.commit.committed_date
        )[-1:][0].name

    def raise_for_namespace_errors(self, tag: SemverTag):

        if not getattr(self.remotes, "origin"):
            raise AttributeError(
                "Please set this repo's origin before running this script"
            )

        head = SemverHead(self)

        if not (head.is_master or head.is_minor_version_of(tag)):
            raise ValueError(
                "HEAD must be at master or a minor release branch, "
                "not {}".format(head)
            )

        if head.is_minor_version_of(tag):

            expected_tag = SemverTag(self.last_tag).minor_number + 1
            if tag != expected_tag:
                raise ValueError(
                    "{} is not a bugfix release of {}. "
                    "Please fix the release version, or push from master to "
                    "make a minor or major release.".format(tag, str(head.ref))
                )

        if str(tag) in self.tags:
            raise ValueError("Version {} already exists.".format(str(tag)))

    def recreate_tag(self, tag, message):

        action = "Creating"
        if tag in self.tags:
            print("Deleting {} tag".format(tag))
            self.delete_tag(self.tags[tag])
            action = "Recreating"
        print("{} {} tag".format(action, tag))
        self.create_tag(tag, message=message)
