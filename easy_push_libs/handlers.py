"""
Copyright (c) 2017 Fernando Collova

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
from easy_push_libs.libs import handle_user_input
from ConfigsTweaker.main import change
import os
from git import Repo
from pathlib import Path


def python(repo, new_tag, setup_path=os.getcwd() + "/setup.py"):
    """Changes the version of ./setup.py.

    In case of having a Python setup.py, the file is updated with the new
    version. If not, the process continue. The only supported location for
    setup.py is in the same folder the script is ran.
    """

    setup_path = Path(setup_path).resolve()

    if not setup_path.exists() or not setup_path.is_file():
        raise ValueError("{} is not a valid setup.py path.".format(setup_path))

    desired_line = 'version="{}",'.format(new_tag)

    print("Changing setup.py version")
    try:
        change(
            mode="change_line",
            trigger="version=",
            input_data=desired_line,
            file=str(setup_path)
        )
    except ValueError:
        change(
            mode="insert_after",
            newline="both",
            trigger="setup(",
            input_data="    " + desired_line,
            file=str(setup_path)
        )

    repo.index.add(["setup.py"])
    repo.index.commit("Bumped setup.py version")


def env_file(repo, new_tag, env_path=os.getcwd() + "/.env"):

    env_path = Path(env_path).resolve()

    if not env_path.exists() or not env_path.is_file():
        raise ValueError("{} is not a valid .env path.".format(env_path))

    project_name = env_path.parts[-2:-1][0]
    desired_line = 'COMPOSE_PROJECT_NAME={}-{}'.format(project_name, new_tag)

    print("Changing .env file COMPOSE_PROJECT_NAME line")
    try:
        change(
            mode="change_line",
            trigger="COMPOSE_PROJECT_NAME=",
            input_data=desired_line,
            file=str(env_path)
        )
    except ValueError:
        change(
            mode="add_line",
            input_data=desired_line,
            file=str(env_path)
        )

    repo.index.add([env_path])
    repo.index.commit("Bumped .env version")
