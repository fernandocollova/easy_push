"""
Copyright (c) 2017 Fernando Collova

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
from argparse import ArgumentParser
import os
from io import StringIO
from sh import ssh_agent, ssh_add
from subprocess import PIPE, run

class UserInterrupt(Exception):

    pass


class sshSession(object):

    def __init__(self, path):
        self.key_path = path
        self.pub_key_path = path + ".pub"
        with StringIO() as buf:
            ssh_agent(_out=buf)
            buf.seek(0)
            out = buf.read().split(";")
            envs = []
            for part in out:
                if "=" in part:
                    envs.append(part.strip().split("="))
            for env in envs:
                os.environ[env[0]] = env[1]
        os.environ.unsetenv("DISPLAY")
        os.environ.unsetenv("SSH_ASKPASS")
        result = run(
            ("ssh-add", self.key_path),
            stdin=PIPE,
            stdout=PIPE,
            stderr=PIPE,
            check=True,
        )

    def close(self):
        ssh_add("-d", self.pub_key_path)
        ssh_agent("-k")

    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        return self.close()


def handle_user_input(prompt, choices):
    user_choice = None
    while user_choice not in choices:
        user_choice = input(prompt)

    if user_choice == "a":
        raise UserInterrupt("Aborting")
    else:
        return user_choice


class EPArgumentParser(ArgumentParser):

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.add_argument(
            '-i', '--identity',
            type=str,
            help="Identity to use",
            dest='identity',
        )

        self.add_argument(
            '-m', '--message',
            type=str,
            help="Tag message",
            dest='message',
        )

        self.add_argument(
            '-p', '--process-python',
            help="Update the version arg in the setup.py file pointed by -s",
            action="store_true",
            dest='process_python',
            default=False
        )

        self.add_argument(
            '-s', '--setup-path',
            help="Path of the setup.py file to process",
            type=str,
            dest='setup_path',
            default=os.getcwd() + "/setup.py"
        )

        self.add_argument(
            '-c', '--compose-env',
            help="Update COMPOSE_PROJECT_NAME in the .env file pointed by -e",
            action="store_true",
            dest='process_env',
            default=False
        )

        self.add_argument(
            '-e', '--env-path',
            help="Path of the .env file, to process",
            type=str,
            dest='env_path',
            default=os.getcwd() + "/.env"
        )
