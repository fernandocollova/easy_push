#!python
"""
Copyright (c) 2017 Fernando Collova

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""

import os
import re
from git import Repo
from easy_push_libs.libs import EPArgumentParser, sshSession
from easy_push_libs import handlers
from easy_push_libs.git_semver import SemverRepo, SemverTag
from ConfigsTweaker.main import change


def perform_push(message, process_python, setup_path, process_env, env_path):

    with SemverRepo(os.getcwd()) as repo:

        try:
            current_tag = SemverTag(repo.last_tag)
        except ValueError:
            message = "The project's last version ({}) is not valid. Please "
            "provide a new version or fix the last one".format(repo.last_tag)
            raise ValueError(message) from None

        new_tag = current_tag.interactive_bump()

        if not message:
            message = str(new_tag)

        repo.raise_for_namespace_errors(new_tag)

        if process_python:
            handlers.python(
                repo=repo,
                new_tag=new_tag,
                setup_path=setup_path,
            )

        if process_env:
            handlers.env_file(
                repo=repo,
                new_tag=new_tag,
                env_path=env_path,
            )

        if new_tag.minor_version not in repo.branches:
            print("Creating {} branch".format(new_tag.minor_version))
            new_branch = repo.create_head(new_tag.minor_version)

        print("Creating {} tag".format(str(new_tag)))
        repo.create_tag(new_tag, message=message)

        repo.recreate_tag(new_tag.minor_version, message)
        repo.recreate_tag(new_tag.major_version, message)

        origin = repo.remote(name='origin')

        print("Pushing code")
        origin.push(repo.active_branch.name)

        print("Pushing tags")
        origin.push(tags=True)


def easy_push_script(*args, **kwargs):
    """Main function entrypoint."""

    with sshSession(kwargs.pop("identity")):

        perform_push(**kwargs)


if __name__ == "__main__":
    try:
        parser = EPArgumentParser()
        args = parser.parse_args()
        easy_push_script(**vars(args))
    except KeyboardInterrupt:
        print("\nA keyboard interrupt caught was caught. Goodbye.")
