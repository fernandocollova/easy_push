import unittest
from easy_push_libs.git_semver import SemverTag


class TestSemverTag(unittest.TestCase):

    def setUp(self):

        self.ok_tag = SemverTag("1.1.1")

    def test_semver_tag_fails_on_bad_version(self):

        with self.assertRaises(ValueError, msg='1.1.a is not  a vlid tag'):
            failing_semver_tag = SemverTag("1.1.a")
