"""
Copyright (c) 2017 Fernando Collova

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
from setuptools import setup

setup(
    name='easy_push',
    version="12.1.0",
    description='easy_push',
    packages=['easy_push_libs'],
    scripts=['easy_push.py'],
    install_requires=["gitpython", "sh"]
)
