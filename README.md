# easy_push

easy_push is a python script allowing you to simplify the branching and tagging of proyects following [semantic versioning](http://semver.org/)

The organization it proposese is basically this:

    master branch
      |
      |
      |
    1.0.0 tag
      |
    1.0.1 tag
      |
    1.0.2 tag
      |\
      | \
      |  \ --> 1.0 branch
      |   \
      |    \
      |   1.0.3 tag and 1.0 tag
      |     |
      |     |
      |   random commit --> last commit of the 1.0 branch
      |
    1.1.0 tag and 1.1 tag
      |
      |
    1.2.0 tag and 1.2 tag and 1 tag
      |
      |
    random commit --> last commit of the master

Or in a more verbose way:

- Each bugfix or X.X.0 release gets it's tag
- If the minor version of a new release it's not the last minor version of the master branch, a minor version branch is created
- The minor version tag follows the last bugfix release of that version
- The major version tag follows the  last bugfix release of that version

All this is done interfacing git, and asking you simple questions before pushing new tags. For example, to push a new version to this proyect one would simply do:

`easy_push.py --move-stable -i ~/.ssh/id_rsa -m "My awesome tag comment"`

And would be greeted with this message:

Last tag was Major.minor.bugfix, and here are some proposed tags from it:
Choose the letter that fits the release type

    Major.minor.(bubfix+1) (b)ugfix release
    Major.(minor+1).0 (m)inor release
    (Major+1).0.0 (M)ajor release

    Or (a) to abort

    Your choice?:

An that's mostly it!

Also, you get a `-p`/`--process-python` argument, that will search and replace the "version=" argument in your setup.py (only if it's in the project root at the moemnt), and match it with the tag used.

# Do you want to contribute?

Just push some code and I'll revew it. And if you have any questions, you can contact me at fernandocollova@gmail.com
Also, don't forget to add your name to the AUTHORS file, and if the contribution was possible because of the resources of anyone or any organization, please thank them in the THANK_YOU file.